package staticlistview.edushare.tw.staticlistview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final String[] arrayList = getResources().getStringArray(R.array.cityList);

        //建立基礎ArrayAdapter
        ArrayAdapter<String> ad = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,arrayList);

        //取得ListView物件
        ListView lv = (ListView)(findViewById(R.id.listView));
        lv.setAdapter(ad);

        //lv.setOnItemClickListener(new ListOnItemClick());
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i("Jerry",arrayList[position]);
            }
        });
    }
}
